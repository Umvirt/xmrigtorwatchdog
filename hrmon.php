#!/usr/bin/env php
<?php
include "config.php";
include "main.php";

$totalhr=0;

date_default_timezone_set($config['timezone']);
$date=date("Y-m-d H:i:s");
echo("Date: ".$date."\n");

$cnt=0;
$bad=0;

foreach($config['workers'] as $w){
$hrstatus="";
$status="down";
$data=getMinerData($w['url'],$w['token']);
if($data){
$cnt++;
$summary=json_decode($data,true);
$hashrate=$summary['hashrate'];
$uptime=$summary['uptime'];
$status="up";

$hr['10s']=$hashrate['total'][0];
$hr['60s']=$hashrate['total'][1];
$hr['15m']=$hashrate['total'][2];
$hr['high']=$hashrate['highest'];
//var_dump($w['url'],$hr);
$hrstatus=" [".join($hr,"/")."] in $uptime s.";

if($hr['60s']){
$totalhr+=$hr['60s'];
}else{
if($uptime>60){
$bad++;
}
}

}

echo $w['name']." : ".$status.$hrstatus."\n";
}

echo "Total hashrate: ".$totalhr."\n";
echo "Active workers: $cnt\n";
echo "Zombie workers: $bad\n";


if($bad){
$cmd="systemctl restart tor";
echo "Runing: $cmd\n";
echo exec($cmd);
sleep(180);
}


echo "\n";
?>
