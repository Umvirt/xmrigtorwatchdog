<?php
function getMinerData($url,$token){
$ch = curl_init($url.'/2/summary');

$authorization = "Authorization: Bearer ".$token; 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch); 
curl_close($ch); 
return $result;
}
